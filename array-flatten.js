var flatten = function(arr, resultArr) {
	var result = resultArr || [];
	for (var i = 0; i < arr.length; i++) {
		var item = arr[i]
		if (Array.isArray(item)) { //ECMAScript 5
			flatten(item, result);
		} else {
			result.push(item);
		}
	}
	return result;
};
