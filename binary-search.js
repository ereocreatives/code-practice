function binarySearch(sortedValues, target) {
  function search(low, high) {
    if (low > high) {
      return null;
    }
    if (sortedValues[low] === target) {
      return low;
    }

    if (sortedValues[high] === target) {
      return high;
    }

    var middle = Math.floor((low + high) / 2);
    var middleElement = sortedValues[middle];

    if (middleElement < target) {
      return search(middle, high - 1);
    } else if (middleElement > target) {
      return search(low + 1, middle);
    }
    return middle;
  }

  // Start our search between the first and last elements of
  //  the array.
  return search(0, sortedValues.length - 1);
}
